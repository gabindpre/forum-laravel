<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'id' => '1',
            'user_id' => '1',
            'content' => 'Message',
            'commentable_id' => '1',
            'commentable_type' => 'App\forum',
            'created_at' => '2019-11-30 22:32:42',
            'updated_at' => '2019-11-30 22:32:4',
        ]);
    }
}
