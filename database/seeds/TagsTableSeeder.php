<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'id' => '1',
            'name' => 'Catégorie 1',
            'slug' => 'categorie1',
            'created_at' => '2019-11-30 22:32:42',
            'updated_at' => '2019-11-30 22:32:4',
        ]);
    }
}
