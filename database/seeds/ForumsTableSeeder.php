<?php

use Illuminate\Database\Seeder;

class ForumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->insert([
            'id' => '1',
            'user_id' => '1',
            'title' => 'Article1',
            'slug' => 'article1',
            'description' => '<p>Description de l article</p>',
			'image' => 'NULL',
            'created_at' => '2019-11-30 22:32:42',
            'updated_at' => '2019-11-30 22:32:4',
        ]);
    }
}
