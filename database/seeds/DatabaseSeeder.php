<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
		$this->call(ForumsTableSeeder::class);
		$this->call(ForumTagTableSeeder::class);
		$this->call(CommentsTableSeeder::class);
		$this->call(ChatTableSeeder::class);
    }
}
