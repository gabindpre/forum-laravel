<?php

use Illuminate\Database\Seeder;

class ForumTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forum_tag')->insert([
            'id' => '1',
            'forum_id' => '1',
            'tag_id' => '1',
        ]);
    }
}
