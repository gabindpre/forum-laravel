<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chat')->insert([
            'title' => Str::random(10),
            'contenu' => Str::random(200),
            'sender_id' => integer(),
            'receiver_id' => integer(),
            'check' => integer(),
            'read' => boolean(),
        ]);
    }
}
