<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

## URL
Route::get('/', function () {
    return view('welcome');
});
Route::get('/presentation', function () {
    return view('presentation');
});
Route::get('/article', function () {
    return view('article');
});


## Route
Route::get('/contact', function () {
    return view('contact');
})->name('contact');
Route::get('/autre', function () {
    return view('autre');
})->name('autre');
Route::get('/welcome', function () {
    return view('welcome');
})->name('autre');
Auth::routes();




Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin_home')->middleware('auth', 'role:Admin');
Route::get('/moderateur', 'ModerateurController@index')->name('moderateur_home')->middleware('auth', 'role:Moderateur');;
Route::get('/autres', 'AutresController@index')->name('autres_home')->middleware('auth', 'role:Autres');
Route::resource('/forum', 'ForumController');

Route::get('/forum/read/{slug}', 'ForumController@show')->name('forumslug');

Route::post('/comment/addComment/{forum}', 'CommentController@addComment')->name('addComment');

Route::post('/comment/replyComment/{comment}', 'CommentController@replyComment')->name('replyComment');

Route::get('/user/{user}', 'ProfilController@index')->name('profile');
Route::get('/user/{user}/edit', 'ProfilController@edit')->name('profile.edit');
Route::patch('/user/{user}', 'ProfilController@update')->name('profile.update');

Route::resource('/tag', 'TagController');

Route::get('/forum/tag/{slug}', 'TagController@index')->name('tagshow');

Route::resource('/chat', 'ChatController');
Route::post('chat/deleteall', 'ChatController@deleteAll')->name('chat_delete_all');

Route::get('/chat/{user}', 'ChatController@index')->name('message');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::ressources('users', 'UserController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
