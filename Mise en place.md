# Mise en place du projet en local

## Prérequis

Installer :
* [Node.js](https://nodejs.org/fr/)
* [WAMP](http://www.wampserver.com/)
* [Composer](https://getcomposer.org/)

## Initialisation

Lancer Wampsever.

Depuis le site [phpMyAdmin](http://localhost/phpmyadmin/), créer une base de donnée en ``utf8_bin`` avec pour nom ``Laravel``.

## Lancement

Ouvrer deux invites de commandes et déplacer à la racine du projet.

Sur le premier invite de commandes, entrer :
```
php artisan serve
```
Sur le second, entrer :
```
php artisan migrate
php artisan db:seed
```

## Tester

Maintenant vous pouvez aller sur le site. http://127.0.0.1:8000/