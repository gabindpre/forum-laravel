<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'contenu', 'sender_name', 'receiver_name' 
    ];

    public static function countMessageNonLus($user){
        $chat = Chat::where('receiver_name', $user)->get();

        $notif = 0;
        foreach($chat as $chats) {
            if ($chats->check == 0) {
                $notif += 1;
            }
        }

        return $notif;
    }
}
