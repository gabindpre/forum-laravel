<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelLike\Traits\CanBeLiked;

class forum extends Model
{

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function hasTag($tagName) 
    {
        foreach ($this->tags as $tag) {
            if ($tag->name === $tagName) {
                return true;
            }
        }
        return false;
    }
}
