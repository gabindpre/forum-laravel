<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Forum;

class ProfilController extends Controller
{
    public function index(User $user)
    {
        $forums = Forum::where('user_id', $user->id)->get();
        return view('profile.index', compact('user', 'forums'));
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);
        return view('profile.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:1024',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $location = public_path('/images');
            $file->move($location, $filename);
            $user->image = $filename;
        }

        $user->save();

        return redirect()->route('profile', ['user' => $user]);
    }
}
