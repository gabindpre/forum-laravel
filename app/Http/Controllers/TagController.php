<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Tag;
use App\Chat;
use Auth;
use DB;

class TagController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();

        $notif = Chat::countMessageNonLus(Auth::user()->name);
        return view('tag.index', compact('tags', 'notif'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $notif = Chat::countMessageNonLus(Auth::user()->name);
        return view('tag.create', compact('tags', 'notif'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = New Tag;
        $tag->name = $request->name;
        $tag->slug = Str::slug($request->name);
        $tag->save();

        return back()->withInfo('Le Tag à été créé !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //$populars = DB::table('forums')
        //               ->join('page-views', 'forums.id', '=', 'page-views.visitable_id')
        //                 ->select(DB::raw('count(visitable_id) as count'), 'forums.id', 'forums.title', 'forums.slug')
        //                 ->groupBy('id', 'title', 'slug')
        //             ->orderBy('count', 'desc')
        //                 ->take(5)
        //                 ->get();


        $tags = Tag::where('id', $slug)
                        ->orWhere('slug', $slug)
                        ->firstOrFail();
                        
        return view('tag.show', compact('tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);
        return view('tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->name = $request->name;
        $tag->slug = Str::slug($request->name);
        $tag->save();

        return redirect()->route('tag.create')->withInfo('Le Tag à été mis à jour !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::find($id);
        $tag->delete();
    
        return redirect()->route('tag.create')->withInfo('Le Tag à été supprimé !');
    }
}
