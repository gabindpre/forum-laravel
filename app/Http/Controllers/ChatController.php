<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Tag;
use Illuminate\Support\Str;
use DB;
use Storage;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user1 = Auth::user()->name;

        if($request->message){
            $e = $request->request->all();
            $tag = $e['message'];
        } else {
            $tag = "0";
        }

        $chat = Chat::all();
        $chat2 = Chat::where('receiver_name', $user1)->get();
    
        $user = User::all();
        $notif = 0;
        foreach($chat2 as $chats) {
            if ($chats->check == 0) {
                $notif += 1;
            }
        }

        return view('chat.index', compact('chat', 'tag', 'notif'));
    }

    /**
     * Show the form for creating a new resource.
     *@param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user1 = Auth::user()->name;
        $chat = Chat::where('sender_name', $user1);
        $users = User::all();
        $notif = Chat::countMessageNonLus(Auth::user()->name);

        return view('chat.create', compact('chat', 'users', 'user1', 'notif'));

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'contenu' => 'required',
            'receiver_name' => 'required',
        ]);

        $chat = New Chat;
        $chat->sender_name = Auth::user()->name;
        $chat->title = $request->title;
        $chat->contenu = $request->contenu;
        $chat->receiver_name = $request->receiver_name[0];
        $chat->check = 0;

        $chat->save();
        
        return back()->withInfo('Enregrister avec Succès !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $chat = Chat::where('sender_name', $user)->orWhere('receiver_name', $user)->get();
        //$chat = Chat::all();
        $check = 2;
        foreach($chat as $chats) {
            $chats->check = 1;
        }
        $chats->save();
        $notif = Chat::countMessageNonLus(Auth::user()->name);

        return view('chat.show', compact('chat', 'user', 'check', 'notif'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(){
        $chats = Chat::all();
        foreach($chats as $chat) {
            $chat->delete();
        }

        return back();
    }
}
