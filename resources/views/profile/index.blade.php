@extends('layouts.app')
@section('title',"user: $user->name")
@section('content')
<div class="container animated fadeInDown">
    <div class="jumbotron" id="tc_jumbotron_profile">
      <div class="card-body">
        <div class="text-center">
         <div class="profile_img"> 
            @if ($user->image !== null)
            <img src="{{asset('images/' .$user->image)}}" style="background: #fff;">
            @else
            <img src="{{asset('images/profile.png')}}" style="background: #fff;">
            @endif
         </div>
          <div id="user_name">
            <h3>{{$user->name}}</h3>
            <p>{{$user->email}}</p>
            @can('update', $user)
                  <a style="background-color: aliceblue" href="{{ route('profile.edit', ['user' => $user->name]) }}" class="btn btn-outline-secondary mt-3 mb-3">Modifier mes information</a>
            @endcan
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="card" style="border: none; text-align: center;">
            <div class="card-header" style="background: #f5f8fa; padding: 0;">
              <div class="footer_sosial_profile">
              <a href="{{ route('forum.create') }}">  <i class="fa fa-edit"></i></a>
              <a href="{{ route('tag.create') }}"><i class="fa fa-list"></i></a> 
              <a href="{{ route('chat.create') }}"><i class="fa fa-search"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  
<div class="container animated fadeInUp">
    <div class="row">
        <div class="col-md-12" id="tc_container_wrap">
           <div class="card" id="tc_paneldefault">
              <div class="card-body" id="tc_panelbody"  style="background: #f9f9f9;"> 
                 <div class="card">
                    <div class="card-header" style="background-color: #3853ef;">
                      <div class="menu_a" style="float: left;">
                      <a href="#">{{$user->name}} Article(s)</a> 
                      </div> 
                 </div> 
              </div> 
          <div class="row">
           <div class="col-md-12">
            <table class="table table-bordered"> 
            <tbody>   
            @forelse($forums as $forum)
               <tr> 
                <td>{{$forum->title}}.., <i style="font-size: 10px; color: #999;"> {{$forum->created_at->diffForHumans()}}, {{$forum->comments->count()}} Comments</i>
                 </td>
                @if(auth()->user()->id == $forum->user_id)
                <td width="10"><a href="{{route('forum.edit', $forum->id)}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a></td>
                <td width="10"> 
                <form action="{{route('forum.destroy', $forum->id)}}" method="post" style="margin: 0;">
                 {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
                 </form> 
                </td>  
                @endif
                <td width="10"><a href="{{route('forumslug', $forum->slug)}}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Voir</a></td>
              </tr>
              @empty
            <br>
            <div class="text-center">
                Pas d'article..
            </div>
              @endforelse
              </tbody>
                  </table>
                   </div> 
                   </div> 
                    <div class="card" style="border: none;">
                    <div class="card-header"></div> 
                    <div class="card-body" style="background: rgb(90, 90, 90)"></div>
                     <div class="card-header"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
@endsection