@extends('layouts.app')

@section('content')
    <div class="container animated fadeInRight" style="margin-bottom: 50px">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Modifier mes information</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('profile.update', ['user' => $user]) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $user->name }}" autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? $user->email }}" autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <a style="margin: 43%" data-toggle="collapse" data-target="#edit_image"><i class="fa fa-image" id="upload_image"></i></a>
                    <div id="edit_image" class="collapse">  
                        <div class="bg">
                            <div class="form-group">
                              <input type="file" class="form-control" name="image" placeholder="image" style="background-color: #f5f8fa;"> 
                            </div>
                            </div>  
                            @if (empty($user->image))
                              <small><i class="fa fa-info-circle"></i>Veuillez à choisir une image</small>
                            @else
                            <div class="form-group">
                               <div class="col-md-4"> 
                               <img src="{{asset('images/'.$user->image)}}" alt="" width="100%">
                               </div>
                           </div>
                           @endif
                          </div><br>

                            <button style="width:100%" type="submit" class="btn btn-primary">Modifier mes informations</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection