@extends('layouts.app')
@section('title','Creer un tag')
@section('content')
<div class="container animated fadeInRight">
     <div class="jumbotron" id="tc_jumbotron">
        <div class="col-md-8 offset-md-2">
          <div class="text-center"><h3 style="color: #fff;">Envoyer un Message</h3></div><hr style="background: #fff"> 
        </div>
      <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card"> 
                <div class="card-body">
                   <form action="{{route('chat.store')}}" method="post">
                      {{csrf_field()}}
                      
                    <div class="form-group">
                        <input disabled="disabled" id="tc_input" type="text" class="form-control" value="{{$user1}}" name="sender_name">
                    </div>
                    <div class="form-group">
                      <input type="text" id="tc_input" class="form-control" name="title" placeholder="Titre.."> 
                    </div>
                    <div class="form-group">
                      <input type="text" id="tc_input" class="form-control" name="contenu" placeholder="Contenu.."> 
                    </div>
                    <div class="form-group">
                    <select name="receiver_name[]" multiple="multiple"  id="tc_input" class="form-control receiver_name">
                        @foreach($users as $user)
                          <option value="{{$user->name}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    </div>  
                    <div class="card">
                      <div class="card-body">   
                        <button style="background-color:black" type="submit" class="btn btn-success btn-block">Enregistrer</button>
                      </div>
                    </div>
               </form>
               </div>
               </div>
<br>

                  </div>
              </div>
        </div>
            </div>
        </div>
    </div>
</div>
@endsection
