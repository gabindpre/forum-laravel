@extends('layouts.app')
@section('title','Tags')
@section('content')
<div class="container">
  <div class="jumbotron" id="tc_jumbotron">
    <div class="card-body"style="color: #fff;border:1px solid #fff;">
        <div class="text-center">
           <h1 style="font-size: 3.5rem;">Chats</h1> 
          <p>Communiquer avec tous les utilisateurs</p> 
      </div>
    </div>
  </div>
</div>  
<div class="container">
    <div class="row">
        <div class="col-md-12" id="tc_container_wrap">
            <div class="card">
                <div class="card-body" style="background: #f9f9f9;"> 
                    <div class="card">
                       <div class="card-header" style="background-color: #2ab27b;padding: 6px 11px 6px 23px;">
                          <div class="menu_a" style="float: left;">
                          <a href="{{route('chat.index')}}">Message</a> 
                          </div>
                          <div class="search" style="margin: 3px;">
                          <div class="col-md-4 float-right" style="padding-right: 0;">
                          <div class="input-group">
                          <input type="text" class="form-control" placeholder="Chercher un message..." style=" margin-right: 3px;background: #f5f8fa;">
                          <span class="input-group-btn">
                          <button class="btn btn-warning" type="button" style="color: #fff;" >Go!</button>
                        </span>
                        </div>
                       </div>
                     </div>
                    </div>
                  </div>  
      <br>
<div class="row" style="margin-top: 50px">
<div class="col-md-12" id="tc_container_wrap">
            <div class="card" id="tc_paneldefault"> 
                <div class="card-body" id="tc_panelbody"  style="background: #f9f9f9;">  
                    <a href="{{route('chat.create')}}" class="btn btn-success btn-block">Nouveau Chat</a>
               <div class="row">

               <div class="col-md-12" style="    padding-right: 0;"><br>
                <table style="width: 98.5%;" class="table table-bordered">
              <thead id="tc_thead">
                <tr>
                  <th scope="col">Derniere(s) Message(s)</th>

                </tr>
              </thead>
              <tbody style="background: #f9f9f9;">
              @foreach($chat as $chats)
              @if ($chats->sender_name == $user || $chats->receiver_name == $user)
              @if ($chats->sender_name == Auth::user()->name && $chats->receiver_name !== Auth::user()->name)
                <tr> 
                <td width="100%">
                <div style="float: right; margin-right: 5%" class="forum_title">
                <h4> <a href="{{route('chat.show', $chats->sender_name)}}">{{$chats->title}}</a></h4>
                <p style="color: black;"> {{$chats->contenu}}</p> 
                <small style="margin-bottom: 0; color: #666">{{$chats->created_at}}</small><br>
            <small style="margin-bottom: 0; color: #666">Vous avez envoyez ce message à : <a href="">{{$chats->receiver_name}}</a></small>
     


             @elseif ($chats->receiver_name == Auth::user()->name && $chats->sender_name !== Auth::user()->name )

               <tr> 
                <td width="453">
                <div style="float: left; margin-left: 5%" class="forum_title">
                <h4> <a href="{{route('chat.show', $chats->sender_name)}}">{{$chats->title}}</a></h4>
                <p style="color: black;"> {{$chats->contenu}}</p> 

            <small style="margin-bottom: 0; color: #666">{{$chats->created_at}}</small><br>
            <small style="margin-bottom: 0; color: #666">Vous avez reçu un message de : <a href="">{{$chats->sender_name}}</a></small>  
              </td>

          

             
             
             
             @endif
             @endif
             @endforeach
            </div>
            </td>
            </tr>
                <?php  ?>

              </tbody>
              
            </table>
            </div>

            

    </div>


      </div>
    </div>
 <br>
@endsection
 