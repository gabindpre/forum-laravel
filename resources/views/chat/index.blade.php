@extends('layouts.app')
@section('title','Tags')
@section('content')
<?php 
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
?>
<div style="z-index: 0" class="container animated fadeInDown">
  <div class="jumbotron" id="tc_jumbotron">
    <div class="card-body"style="color: #fff;border:1px solid #fff;">
        <div class="text-center">
           <h1 style="font-size: 3.5rem;">Chats</h1> 
          <p>Communiquer avec tous les utilisateurs</p> 
      </div>
    </div>
  </div>
</div>  
<div class="container animated fadeInRight">
    <div class="row">
        <div class="col-md-12" id="tc_container_wrap">
            <div class="card">
                <div class="card-body" style="background: #f9f9f9;"> 
                    <div class="card">
                       <div class="card-header" style="background-color: #556080;padding: 6px 11px 6px 23px;">
                          <div class="menu_a" style="float: left;">
                          <a href="{{route('chat.index')}}">Message</a> 
                          </div>
                          <div class="search" style="margin: 3px;">
                          <div class="col-md-4 float-right" style="padding-right: 0;">
                          <div class="input-group">
                          <input type="text" class="form-control" placeholder="Chercher un message..." style=" margin-right: 3px;background: #f5f8fa;">
                          <span class="input-group-btn">
                          <button class="btn btn-warning" type="button" style="color: #fff;" >Go!</button>
                        </span>
                        </div>
                       </div>
                     </div>
                    </div>
                  </div>  
      <br>
      @if($iPhone || $iPod || $iPad || $Android)
            <div style="margin-top: -20px" class="col-md-4"> <br>
               <div class="card">
                <div class="card-header" style="background: #9F9F9F; color: #fff; padding: 8px 1.25rem;">Conversation</div>
                <div class="list-group">

                 <a href="{{ route('chat.index', ['message'=> '1' ]) }}"  class="list-group-item <?= $tag === '1' ? 'active' : ''  ?> " id="index_hover">Message envoyé(s)
                </a>
                <a href="{{ route('chat.index', ['message'=> '2']) }}"  class="list-group-item <?= $tag === '2' ? 'active' : ''  ?> " id="index_hover">Message reçu(s)
                </a> 
                <a href="{{ route('chat.index', ['message'=> '3' ]) }}"  class="list-group-item <?= $tag === '3' ? 'active' : ''  ?> " id="index_hover">Tous les envoyé(s)
                </a> 

                </div>
                </div>
                </div>
                <br>
                @else
                @endif
<div class="row" style="margin-top: 50px">
<div class="col-md-12" id="tc_container_wrap">
            <div class="card" id="tc_paneldefault"> 
                <div class="card-body" id="tc_panelbody"  style="background: #f9f9f9;">  
                    <a href="{{route('chat.create')}}" class="btn btn-success btn-block">Nouveau Chat</a>
                    <a class="btn btn-danger btn-block" style="background-color: #FA9494" href="{{ route('chat_delete_all') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('destroy-chat').submit();" style="color: #444;">
                        {{ __('Supprimer') }}
                    </a>
                    <form id="destroy-chat" action="{{ route('chat_delete_all') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
               <div class="row">
               @if($iPhone || $iPod || $iPad || $Android)
               <div class="col-md-8"><br>
               @else
               <div class="col-md-8" style="    padding-right: 0;"><br>
               @endif
                <table class="table table-bordered">
              <thead id="tc_thead">
                <tr>
                  <th scope="col">Derniere(s) Message(s)</th>
                  <th scope="col">Utilisateur</th>

                </tr>
              </thead>
              <tbody style="background: #f9f9f9;">
              @foreach($chat as $chats)
              @if ($chats->sender_name == Auth::user()->name && $chats->receiver_name !== Auth::user()->name)
              @if($tag == '1' || $tag == '3' || $tag == '0')
                <tr> 
                <td width="453">
                <div class="forum_title">
                <h4> <a href="{{route('chat.show', $chats->receiver_name)}}">{{$chats->title}}</a></h4>
                <p> {{$chats->contenu}}</p> 
              </td>
              <td>
            <div class="forum_by">
            <small style="margin-bottom: 0; color: #666">{{$chats->created_at}}</small><br>
            <small style="margin-bottom: 0; color: #666">Vous avez envoyez ce message à : <a href="">{{$chats->receiver_name}}</a></small>
             
             @endif
             @elseif ($chats->receiver_name == Auth::user()->name && $chats->sender_name !== Auth::user()->name )

            @if ($tag == '3' || $tag == '2' || $tag == '0')
               <tr> 
                <td width="453">
                <div class="forum_title">
                <h4> <a href="{{route('chat.show', $chats->sender_name)}}">{{$chats->title}}</a></h4>
                <p> {{$chats->contenu}}</p>
                @if ($chats->check == 0)
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                @endif
              </td>
              <td>
            <div class="forum_by">
            <small style="margin-bottom: 0; color: #666">{{$chats->created_at}}</small><br>
            <small style="margin-bottom: 0; color: #666">Vous avez reçu un message de : <a href="">{{$chats->sender_name}}</a></small>             
             @endif
             
             
             
             @endif
             @endforeach
            </div>
            </td>
            </tr>
                <?php  ?>

              </tbody>
              
            </table>
            </div>
            @if($iPhone || $iPod || $iPad || $Android)
            @else
            <div class="col-md-4"> <br>
               <div class="card">
                <div class="card-header" style="background: #9F9F9F; color: #fff; padding: 8px 1.25rem;">Conversation</div>
                <div class="list-group">

                 <a href="{{ route('chat.index', ['message'=> '1' ]) }}"  class="list-group-item <?= $tag === '1' ? 'active' : ''  ?> " id="index_hover">Message envoyé(s)
                </a>
                <a href="{{ route('chat.index', ['message'=> '2']) }}"  class="list-group-item <?= $tag === '2' ? 'active' : ''  ?> " id="index_hover">Message reçu(s)
                </a> 
                <a href="{{ route('chat.index', ['message'=> '3' ]) }}"  class="list-group-item <?= $tag === '3' ? 'active' : ''  ?> " id="index_hover">Tous les envoyé(s)
                </a> 

                </div>
                </div>
                </div>
                @endif
                </div>


                <hr style="margin-top: 0;"> 
                <div class="card">
                <div class="card-header"></div>
                <div class="card-body" style="background: rgb(90, 90, 90)"></div>
                <div class="card-header"></div>
                </div>
            </div>
            

    </div>


      </div>
    </div>
 <br>
@endsection
 