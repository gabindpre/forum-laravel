@extends('layouts.app')
@section('title','Forum')
@section('content')
<?php 
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
?>

<div class="container animated fadeInLeft">
  <div class="jumbotron" id="tc_jumbotron">
    <div class="card-body" id="xx" style="color: #fff;     border:1px solid #fff;">
        <div class="text-center"> 
           <h1 style="    font-size: 3.5rem;">FORUM</h1> 
        <p></p>  
          </div>
        </div> 
      </div> 
    </div>
    @if($iPhone || $iPod || $iPad || $Android)
    
                <div class="card-body" id="tc_panelbody"  style="background: #f9f9f9; margin-left: -20px;">  
               <div class="row">
               <div class="col-md-8" style="    padding-right: 0;">

               <div class="col-md-4">
               @if($iPhone || $iPod || $iPad || $Android)
               @else
               <br>
               @endif
                <a href="{{route('forum.create')}}" class="btn btn-success btn-block">Creer un Article</a><br>
               <div class="card" style="margin-bottom: 100px">
                <div class="card-header" style="background: #9F9F9F; color: #fff; padding: 8px 1.25rem;">Categorie(s)</div>
                <div class="list-group">
                <?php $res = []; ?>
                @foreach($tags as $tag)
                 <a href="{{ route('forum.index', ['tag'=> $tag->name ]) }}"  class="list-group-item <?= $tag->name === $tagName ? 'active' : ''  ?>" id="index_hover">{{$tag->name}}
                </a> 
                @endforeach
                <a href="{{ route('forum.index') }}"  class="list-group-item <?= $tag->name === "" ? 'active' : ''  ?>" id="index_hover">Toutes les catégories
                </a> 
                </div>
                </div>
                </div>
            @else
            @endif


            @if ($iPhone || $iPod || $iPad || $Android)
            <div style="margin-top: -70px" class="container animated fadeInRight"> 
              @else
              <div class="container animated fadeInRight"> 
              <div class="row">
             <div class="col-md-12" id="tc_container_wrap">
            @endif
            <div class="card" id="tc_paneldefault"> 
                <div class="card-body" id="tc_panelbody"  style="background: #f9f9f9;">  
               <div class="row">
               @if ($iPhone || $iPod || $iPad || $Android)
              @else
              <div class="col-md-8" style="    padding-right: 0;"><br>
               @endif
                <table class="table table-bordered">
              <thead id="tc_thead">
                <tr>
                  <th scope="col">Article</th>
                  <th scope="col">Commentaires</th>
                  <th scope="col">Utilisateur</th>
                </tr>
              </thead>
              <tbody style="background: #f9f9f9;">
                @foreach($forums as $forum)
                <tr> 
                <td width="553">
                <div class="forum_title">
                <h4> <a href="{{route('forumslug', $forum->slug)}}">{{ Str::limit($forum->title, 45)}}</a></h4>
                <p>  {!!  strip_tags( Str::limit($forum->description, 60) )!!}</p> 
                <div>
                @if (empty($forum->image))
                  <div style="margin-left: -20px">
                  @foreach($forum->tags as $tag)
                    <div style="margin-left: 4%">
                      <a href="{{ route('forum.index', ['tag'=> $tag->name ]) }}" style="background-color: grey; top: initial; right: auto" class="badge badge-success tag_label">{{$tag->name}}</a>
                    <div>
                  @endforeach
                  <div>
                @else
                  <div style="background-color: grey; top: initial;right: auto" class="badge badge-success tag_label_image"><i class="fa fa-image"></i></div> 
                  @foreach($forum->tags as $tag)
                    <div style="margin-left: 4%">
                      <a href="{{ route('forum.index', ['tag'=> $tag->name ]) }}" style="background-color: grey; top: initial; margin-left: 10px; right:auto" class="badge badge-success tag_label">{{$tag->name}}</a>
                    <div>
                  @endforeach
                @endif
                </div>

                </div> </div>
              </td>
              <td style="text-align: center"><small> {{$forum->comments->count()}}</small></td>
              <td>
            <div class="forum_by">
            <small style="margin-bottom: 0; color: #666">{{$forum->created_at}}</small>
            <small>by <a href="{{ route('profile', $forum->user->name ) }}">{{ $forum->user->name }}</a></small>
            @if ($forum->user->image !== null)<br>
            <img src="{{asset('images/' .$forum->user->image)}}" style="background: #fff; width: 35%; border-radius: 50%; height: 50%;">
            @endif     
            </div>
            </td>
            </tr>
                <?php  ?>
                @endforeach
              </tbody>
            </table>
            @if ($iPhone || $iPod || $iPad || $Android)
              @else
              </div>
            @endif
              @if ($iPhone || $iPod || $iPad || $Android)
              @else
                <div class="col-md-4"> <br>
                <a href="{{route('forum.create')}}" class="btn btn-success btn-block">Creer un Article</a><br>
               <div class="card" style="margin-bottom: 100px">
                <div class="card-header" style="background: #9F9F9F; color: #fff; padding: 8px 1.25rem;">Categorie(s)</div>
                <div class="list-group">
                <?php $res = []; ?>
                @if ($tags->isEmpty())
                @else
            
                  @foreach($tags as $tag)
                  <a href="{{ route('forum.index', ['tag'=> $tag->name ]) }}"  class="list-group-item <?= $tag->name === $tagName ? 'active' : ''  ?>" id="index_hover">{{$tag->name}}
                  </a> 
                  @endforeach
                  <a href="{{ route('forum.index') }}"  class="list-group-item <?= $tag->name === "" ? 'active' : ''  ?>" id="index_hover">Toutes les catégories
                  </a>
                @endif
                </div>
                </div>
                </div>
              @endif
                </div>
                <hr style="margin-top: 0;"> 
                <div class="card">
                <div class="card-header"></div>
                <div class="card-body" style="background: rgb(90, 90, 90)"></div>
                <div class="card-header"></div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div><br><br>

@endsection
 