@extends('layouts.app')
@section('title','Tags')
@section('content')
<div class="container animated fadeInDown">
  <div class="jumbotron" id="tc_jumbotron">
    <div class="card-body"style="color: #fff;border:1px solid #fff;">
        <div class="text-center">
           <h1 style="font-size: 3.5rem;">Tags</h1> 
      </div>
    </div>
  </div>
</div>  
<div class="container animated fadeInRight">

    <div class="row">
        <div class="col-md-12" id="tc_container_wrap">
            <div class="card">
            <a style="margin:25px; width: 95%" href="{{route('tag.create')}}" class="btn btn-success btn-block">Créer un Tag</a><br>
      <br>

<div class="row">
   <div class="col-md-12">
     @foreach($tags as $tag)
       <a style="margin-right: 2%; margin-left: 2%; margin-bottom: 2%; width: 20%" href="{{ route('forum.index', ['tag'=> $tag->name ]) }}" class="btn btn-success btn-sm">{{$tag->name}}</a>
     @endforeach
    </div>
       <div class="col-md-4">
           </div>
           </div><br><br>
            <div class="card">
              <div class="card-header"></div>
               <div class="card-body" style="background: rgb(90, 90, 90)"></div>
              <div class="card-header"></div>
             </div>
            </div>
           </div>
         </div>
      </div>
    </div>
 <br>
 </div>  
@endsection
 