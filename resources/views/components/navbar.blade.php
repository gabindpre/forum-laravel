<nav style="position: fixed; width: 100%; margin-bottom: 50px; z-index:1" class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="/">Accueil</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/forum">Forum</a>
    </li>  
    <li class="nav-item active">
      <a class="nav-link" href="/contact">Contact</a>
    </li>    



  </ul>


  @if (Route::has('login'))
    <div class="top-right links">
        @auth
            <a href="{{ url('/home') }}">Home</a>
        @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
                <a href="{{ route('register') }}">Register</a>
            @endif
        @endauth
    </div>
  @endif
</nav>

